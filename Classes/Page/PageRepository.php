<?php
/**
 * This Software is the property of polargold and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.polargold.de
 * @copyright (C) polargold 2016
 * @author    Marcos Fadul <Marcos _AT_ polargold.de>
 */
namespace PG\PgFalTranslation\Page;

class PageRepository extends \TYPO3\CMS\Frontend\Page\PageRepository
{
    /**
     * Creates language-overlay for records in general (where translation is found
     * in records from the same table)
     *
     * @param string $table Table name
     * @param array $row Record to overlay. Must containt uid, pid and $table]['ctrl']['languageField']
     * @param int $sys_language_content Pointer to the sys_language uid for content on the site.
     * @param string $OLmode Overlay mode. If "hideNonTranslated" then records without translation will not be returned  un-translated but unset (and return value is FALSE)
     * @throws \UnexpectedValueException
     * @return mixed Returns the input record, possibly overlaid with a translation.  But if $OLmode is "hideNonTranslated" then it will return FALSE if no translation is found.
     */
    public function getRecordOverlay($table, $row, $sys_language_content, $OLmode = '')
    {
        if (is_array($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_page.php']['getRecordOverlay'])) {
            foreach ($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_page.php']['getRecordOverlay'] as $classRef) {
                $hookObject = GeneralUtility::getUserObj($classRef);
                if (!$hookObject instanceof PageRepositoryGetRecordOverlayHookInterface) {
                    throw new \UnexpectedValueException(
                        $classRef . ' must implement interface ' . PageRepositoryGetRecordOverlayHookInterface::class,
                        1269881658
                    );
                }
                $hookObject->getRecordOverlay_preProcess($table, $row, $sys_language_content, $OLmode, $this);
            }
        }
        if ($row['uid'] > 0 && ($row['pid'] > 0 || in_array($table, $this->tableNamesAllowedOnRootLevel, true))) {
            if ($GLOBALS['TCA'][$table] && $GLOBALS['TCA'][$table]['ctrl']['languageField'] && $GLOBALS['TCA'][$table]['ctrl']['transOrigPointerField']) {
                // Return record for ALL languages untouched
                // TODO: Fix call stack to prevent this situation in the first place
                if (!$GLOBALS['TCA'][$table]['ctrl']['transOrigPointerTable'] && (int)$row[$GLOBALS['TCA'][$table]['ctrl']['languageField']] !== -1) {
                    // Will not be able to work with other tables (Just didn't implement it yet;
                    // Requires a scan over all tables [ctrl] part for first FIND the table that
                    // carries localization information for this table (which could even be more
                    // than a single table) and then use that. Could be implemented, but obviously
                    // takes a little more....) Will try to overlay a record only if the
                    // sys_language_content value is larger than zero.
                    if ($sys_language_content > 0) {
                        // Must be default language, otherwise no overlaying
                        if ((int)$row[$GLOBALS['TCA'][$table]['ctrl']['languageField']] === 0) {
                            // Select overlay record:
                            $res = $this->getDatabaseConnection()->exec_SELECTquery(
                                '*',
                                $table,
                                'pid=' . (int)$row['pid'] . ' AND ' . $GLOBALS['TCA'][$table]['ctrl']['languageField'] . '=' . (int)$sys_language_content . ' AND ' . $GLOBALS['TCA'][$table]['ctrl']['transOrigPointerField'] . '=' . (int)$row['uid'] . $this->enableFields(
                                    $table
                                ),
                                '',
                                '',
                                '1'
                            );

                            $olrow = $this->getDatabaseConnection()->sql_fetch_assoc($res);
                            $this->getDatabaseConnection()->sql_free_result($res);
                            $this->versionOL($table, $olrow);
                            // Merge record content by traversing all fields:
                            if (is_array($olrow)) {
                                if (isset($olrow['_ORIG_uid'])) {
                                    $row['_ORIG_uid'] = $olrow['_ORIG_uid'];
                                }
                                if (isset($olrow['_ORIG_pid'])) {
                                    $row['_ORIG_pid'] = $olrow['_ORIG_pid'];
                                }
                                foreach ($row as $fN => $fV) {
                                    if ($fN !== 'uid' && $fN !== 'pid' && isset($olrow[$fN])) {
                                        if ($this->shouldFieldBeOverlaid($table, $fN, $olrow[$fN])) {
                                            $row[$fN] = $olrow[$fN];
                                        }
                                    } elseif ($fN === 'uid') {
                                        $row['_LOCALIZED_UID'] = $olrow['uid'];
                                    }
                                }
                            } elseif ($OLmode === 'hideNonTranslated' && (int)$row[$GLOBALS['TCA'][$table]['ctrl']['languageField']] === 0
                                // @todo: do it better. Hack = Patch.
                                && $table != 'sys_file_reference'
                            ) {
                                // Unset, if non-translated records should be hidden. ONLY done if the source
                                // record really is default language and not [All] in which case it is allowed.
                                unset($row);
                            }
                        } elseif ($sys_language_content != $row[$GLOBALS['TCA'][$table]['ctrl']['languageField']]) {
                            unset($row);
                        }
                    } else {
                        // When default language is displayed, we never want to return a record carrying
                        // another language!
                        if ($row[$GLOBALS['TCA'][$table]['ctrl']['languageField']] > 0) {
                            unset($row);
                        }
                    }
                }
            }
        }
        if (is_array($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_page.php']['getRecordOverlay'])) {
            foreach ($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_page.php']['getRecordOverlay'] as $classRef) {
                $hookObject = GeneralUtility::getUserObj($classRef);
                if (!$hookObject instanceof PageRepositoryGetRecordOverlayHookInterface) {
                    throw new \UnexpectedValueException(
                        $classRef . ' must implement interface ' . PageRepositoryGetRecordOverlayHookInterface::class,
                        1269881659
                    );
                }
                $hookObject->getRecordOverlay_postProcess($table, $row, $sys_language_content, $OLmode, $this);
            }
        }
        return $row;
    }
}

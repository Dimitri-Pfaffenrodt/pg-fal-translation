<?php
namespace PG\PgFalTranslation\Persistence\Generic\Mapper;

use TYPO3\CMS\Extbase\DomainObject\DomainObjectInterface;
use TYPO3\CMS\Extbase\Persistence;

class DataMapper extends \TYPO3\CMS\Extbase\Persistence\Generic\Mapper\DataMapper
{
    /**
     * Builds and returns the prepared query, ready to be executed.
     *
     * @param DomainObjectInterface $parentObject
     * @param string $propertyName
     * @param string $fieldValue
     * @param bool $forceTranslationOverlay
     * @return Persistence\QueryInterface
     */
    protected function getPreparedQuery(
        \TYPO3\CMS\Extbase\DomainObject\DomainObjectInterface $parentObject,
        $propertyName,
        $fieldValue = '',
        $forceTranslationOverlay = false
    ) {
        $columnMap = $this->getDataMap(get_class($parentObject))->getColumnMap($propertyName);
        $type = $this->getType(get_class($parentObject), $propertyName);
        $query = $this->queryFactory->create($type);
        $query->getQuerySettings()->setRespectStoragePage(false);
        $query->getQuerySettings()->setRespectSysLanguage(false);
        if ($columnMap->getTypeOfRelation() === ColumnMap::RELATION_HAS_MANY) {
            if ($columnMap->getChildSortByFieldName() !== null) {
                $query->setOrderings(
                    [$columnMap->getChildSortByFieldName() => Persistence\QueryInterface::ORDER_ASCENDING]
                );
            }
        } elseif ($columnMap->getTypeOfRelation() === ColumnMap::RELATION_HAS_AND_BELONGS_TO_MANY) {
            $query->setSource($this->getSource($parentObject, $propertyName));
            if ($columnMap->getChildSortByFieldName() !== null) {
                $query->setOrderings(
                    [$columnMap->getChildSortByFieldName() => Persistence\QueryInterface::ORDER_ASCENDING]
                );
            }
        }
        $query->matching(
            $this->getConstraint(
                $query,
                $parentObject,
                $propertyName,
                $fieldValue,
                $columnMap->getRelationTableMatchFields(),
                $forceTranslationOverlay
            )
        );
        return $query;
    }

    /**
     * Builds and returns the constraint for multi value properties.
     *
     * @param Persistence\QueryInterface $query
     * @param DomainObjectInterface $parentObject
     * @param string $propertyName
     * @param string $fieldValue
     * @param array $relationTableMatchFields
     * @param bool $forceTranslationOverlay
     * @return \TYPO3\CMS\Extbase\Persistence\Generic\Qom\ConstraintInterface $constraint
     */
    protected function getConstraint(
        Persistence\QueryInterface $query,
        DomainObjectInterface $parentObject,
        $propertyName,
        $fieldValue = '',
        $relationTableMatchFields = [],
        $forceTranslationOverlay = false
    ) {
        $columnMap = $this->getDataMap(get_class($parentObject))->getColumnMap($propertyName);
        if ($columnMap->getParentKeyFieldName() !== null) {

            $isRelationsOverriddenByTranslation = false;
            if (method_exists($columnMap, 'isRelationsOverriddenByTranslation')) {
                $isRelationsOverriddenByTranslation = $columnMap->isRelationsOverriddenByTranslation();
            }

            if ($forceTranslationOverlay || $isRelationsOverriddenByTranslation) {
                $relatedTranslations = 0;
                if (!$forceTranslationOverlay) {
                    $relatedTranslations = $this->countRelated($parentObject, $propertyName, $fieldValue, true);
                }
                if ($relatedTranslations > 0 || $forceTranslationOverlay) {
                    $constraint = $query->equals(
                        $columnMap->getParentKeyFieldName(),
                        $parentObject->_getProperty('_localizedUid')
                    );
                } else {
                    $constraint = $query->equals($columnMap->getParentKeyFieldName(), $parentObject);
                }
            } else {
                $constraint = $query->equals($columnMap->getParentKeyFieldName(), $parentObject);
            }
            if ($columnMap->getParentTableFieldName() !== null) {
                $constraint = $query->logicalAnd(
                    $constraint,
                    $query->equals(
                        $columnMap->getParentTableFieldName(),
                        $this->getDataMap(get_class($parentObject))->getTableName()
                    )
                );
            }
        } else {
            $constraint = $query->in('uid', \TYPO3\CMS\Core\Utility\GeneralUtility::intExplode(',', $fieldValue));
        }
        if (!empty($relationTableMatchFields)) {
            foreach ($relationTableMatchFields as $relationTableMatchFieldName => $relationTableMatchFieldValue) {
                $constraint = $query->logicalAnd(
                    $constraint,
                    $query->equals($relationTableMatchFieldName, $relationTableMatchFieldValue)
                );
            }
        }

        return $constraint;
    }

    /**
     * Builds and returns the source to build a join for a m:n relation.
     *
     * @param DomainObjectInterface $parentObject
     * @param string $propertyName
     * @return \TYPO3\CMS\Extbase\Persistence\Generic\Qom\SourceInterface $source
     */
    protected function getSource(DomainObjectInterface $parentObject, $propertyName)
    {
        $columnMap = $this->getDataMap(get_class($parentObject))->getColumnMap($propertyName);
        $left = $this->qomFactory->selector(null, $columnMap->getRelationTableName());
        $childClassName = $this->getType(get_class($parentObject), $propertyName);
        $right = $this->qomFactory->selector($childClassName, $columnMap->getChildTableName());
        $joinCondition = $this->qomFactory->equiJoinCondition(
            $columnMap->getRelationTableName(),
            $columnMap->getChildKeyFieldName(),
            $columnMap->getChildTableName(),
            'uid'
        );
        $source = $this->qomFactory->join(
            $left,
            $right,
            Persistence\Generic\Query::JCR_JOIN_TYPE_INNER,
            $joinCondition
        );
        return $source;
    }

    /**
     * Returns the given result as property value of the specified property type.
     *
     * @param DomainObjectInterface $parentObject
     * @param string $propertyName
     * @param mixed $result The result
     * @return mixed
     */
    public function mapResultToPropertyValue(DomainObjectInterface $parentObject, $propertyName, $result)
    {
        $propertyValue = null;
        if ($result instanceof Persistence\Generic\LoadingStrategyInterface) {
            $propertyValue = $result;
        } else {
            $propertyMetaData = $this->reflectionService->getClassSchema(get_class($parentObject))->getProperty(
                $propertyName
            );
            if (in_array(
                $propertyMetaData['type'],
                ['array', 'ArrayObject', 'SplObjectStorage', \TYPO3\CMS\Extbase\Persistence\ObjectStorage::class],
                true
            )) {
                $objects = [];
                foreach ($result as $value) {
                    $objects[] = $value;
                }
                if ($propertyMetaData['type'] === 'ArrayObject') {
                    $propertyValue = new \ArrayObject($objects);
                } elseif (in_array(
                    $propertyMetaData['type'],
                    [\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class],
                    true
                )) {
                    $propertyValue = new Persistence\ObjectStorage();
                    foreach ($objects as $object) {
                        $propertyValue->attach($object);
                    }
                    $propertyValue->_memorizeCleanState();
                } else {
                    $propertyValue = $objects;
                }
            } elseif (strpbrk($propertyMetaData['type'], '_\\') !== false) {
                // DebuggerUtility::var_dump($propertyMetaData, '$propertyMetaData');
                if (is_object($result) && $result instanceof Persistence\QueryResultInterface) {
                    $propertyValue = $result->getFirst();
                } else {
                    $propertyValue = $result;
                }
            }
        }
        return $propertyValue;
    }

    /**
     * Counts the number of related objects assigned to a property of a parent object
     *
     * @param DomainObjectInterface $parentObject The object instance this proxy is part of
     * @param string $propertyName The name of the proxied property in it's parent
     * @param mixed $fieldValue The raw field value.
     * @param bool $forceTranslationOverlay
     * @return int
     */
    public function countRelated(
        DomainObjectInterface $parentObject,
        $propertyName,
        $fieldValue = '',
        $forceTranslationOverlay = false
    ) {
        $query = $this->getPreparedQuery($parentObject, $propertyName, $fieldValue, $forceTranslationOverlay);
        return $query->execute()->count();
    }
}

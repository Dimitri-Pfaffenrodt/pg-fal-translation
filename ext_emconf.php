<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'FAL translation',
    'description' => 'Work around https://forge.typo3.org/issues/57272',
    'category' => 'misc',
    'author' => 'Marcos Fadul & Peter Dockhorn',
    'author_email' => 'dev@polargold.de',
    'state' => 'stable',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '1.0.1',
    'constraints' => [
        'depends' => [
            'typo3' => '7.6.0-7.9.99',
        ],
        'conflicts' => [
        ],
        'suggests' => [
        ],
    ],
];

<?php
/**
 * This Software is the property of polargold and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.polargold.de
 * @copyright (C) polargold 2016
 * @author    Marcos Fadul <Marcos _AT_ polargold.de>
 */
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Frontend\Page\PageRepository::class] = [
    'className' => \PG\PgFalTranslation\Page\PageRepository::class
];
